import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

import { PhotoComponent } from 'src/app/photo/photo/photo.component';
import { PhotoListComponent } from 'src/app/photo/photo-list/photo-list.component';
import { PhotoFormComponent } from 'src/app/photo/photo-form/photo-form.component';
import { PhotoListPhotosComponent } from 'src/app/photo/photo-list/photo-list-photos/photo-list-photos.component';
import { FilterByDescriptionPipe } from 'src/app/photo/photo-list/pipe/filter-by-description.pipe';
import { LoadButtonComponent } from 'src/app/photo/photo-list/load-button/load-button.component';

@NgModule({
  declarations: [
    PhotoComponent,
    PhotoListComponent,
    PhotoFormComponent,
    PhotoListPhotosComponent,
    FilterByDescriptionPipe,
    LoadButtonComponent
  ],
  exports:[
    PhotoComponent,
    PhotoListComponent,
    PhotoFormComponent,
    PhotoListPhotosComponent,
    FilterByDescriptionPipe,
    LoadButtonComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule
  ]
})
export class PhotoModule { }
