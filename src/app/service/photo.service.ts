import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Photo } from '../interface/photo';

const API = 'http://localhost:3000'

@Injectable({
  providedIn: 'root'
})
export class PhotoService {

  constructor(private http: HttpClient) { }

  listFromUser(userName: string) {  //Substitued for 'paginatedListFromUser()'. 
    return this.http.get<Photo[]>(API + '/' + userName + '/photos');
  }

  paginatedListFromUser(userName: string, pageNumber: number) {
    const parameters = new HttpParams().append('page', pageNumber.toString());
    return this.http.get<Photo[]>(API + '/' + userName + '/photos', { params: parameters });  //Could also be '{ params }' if 'parameters' was also called 'params', as the JS
  }                                                                                           //understands that if a key-value set have the same name, it can be omitted.
}
