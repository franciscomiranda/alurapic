import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';

import { Photo } from 'src/app/interface/photo';
import { PhotoService } from 'src/app/service/photo.service';

@Injectable({
    providedIn: 'root'
})
export class PhotoListResolver implements Resolve<Observable<Photo[]>> {

    constructor(private photoService: PhotoService) { }

    resolve(activatedRouteSnapshot: ActivatedRouteSnapshot, routerStateSnapshot: RouterStateSnapshot): Observable<Photo[]> {
        const userName = activatedRouteSnapshot.params.userName;
        return this.photoService.paginatedListFromUser(userName, 1);
    }

}