import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PhotoListPhotosComponent } from './photo-list-photos.component';

describe('PhotoListPhotosComponent', () => {
  let component: PhotoListPhotosComponent;
  let fixture: ComponentFixture<PhotoListPhotosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhotoListPhotosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhotoListPhotosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
