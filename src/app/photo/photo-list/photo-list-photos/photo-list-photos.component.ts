import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Photo } from 'src/app/interface/photo';

@Component({
  selector: 'app-photo-list-photos',
  templateUrl: './photo-list-photos.component.html',
  styleUrls: ['./photo-list-photos.component.scss']
})
export class PhotoListPhotosComponent implements OnChanges {

  constructor() { }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.childPhotoList) {
      this.rows = this.groupColumns(this.childPhotoList);
    }
  }

  @Input() childPhotoList = [{}] as Photo[];
  rows = [] as any[];

  groupColumns(photoList: Photo[]) {
    const newRows = [];
    for (let i = 0; i < photoList.length; i += 3) {
      newRows.push(photoList.slice(i, i + 3));
    }
    return newRows;
  }
}
