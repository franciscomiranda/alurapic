import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { debounceTime } from 'rxjs/operators';
import { Subject } from 'rxjs';

import { Photo } from 'src/app/interface/photo';
import { PhotoService } from 'src/app/service/photo.service';


@Component({
  selector: 'app-photo-list',
  templateUrl: './photo-list.component.html',
  styleUrls: ['./photo-list.component.scss']
})
export class PhotoListComponent implements OnInit, OnDestroy {

  constructor(
    private photoService: PhotoService,
    private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.userName = this.activatedRoute.snapshot.params.userName;
    this.parentPhotoList = this.activatedRoute.snapshot.data.photos;  //Fetches the data from the resolver 'photos' attribute in app-routing.
  }                                                                   //Could also be referenced as '.data['photos'];'.

  ngOnDestroy() {
    this.debounce.unsubscribe(); //Avoiding memory leak of an 'Observable' that never reaches or calls its 'complete'.
  }

  title = 'alurapic';
  userName: string;
  parentPhotoList = [{}] as Photo[];

  searchFilter: string = '';
  debounce: Subject<string> = new Subject<string>();

  hasMore: boolean = true;
  currentPage: number = 1;

  updateSearchFilter(eventValue: string) {
    this.debounce.next(eventValue); //Emits a new value ('eventValue') for the subject ('debounce').
    this.debounce                   //Waits 400ms upon the emition of 'debounce' to proceed with the subscription verification.
      .pipe(debounceTime(400))      //'Subject' allows to emit and subscribe to values. For comparision, 'Observable' only allows subscriptions.
      .subscribe(searchFilter =>
        this.searchFilter = searchFilter);
  }

  loadMore() {
    this.photoService
      .paginatedListFromUser(this.userName, ++this.currentPage)
      .subscribe(photoList => {
        this.parentPhotoList = this.parentPhotoList.concat(photoList);
        if (!photoList.length) {
          this.hasMore = false;
        }
      });
  }
}
