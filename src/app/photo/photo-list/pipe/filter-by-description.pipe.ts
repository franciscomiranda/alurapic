import { Pipe, PipeTransform } from '@angular/core';
import { Photo } from 'src/app/interface/photo';

@Pipe({
  name: 'filterByDescription'
})
export class FilterByDescriptionPipe implements PipeTransform {

  transform(photoList: Photo[], descriptionQuery: string) {
    descriptionQuery = descriptionQuery.trim().toLowerCase();
    if (descriptionQuery) {

      return photoList
        .filter(photo =>
          photo.description
            .toLowerCase()
            .includes(descriptionQuery)
        );
    } else {
      return photoList;
    }
  }

  /*          //PipeTransform 'transform' method as its standard implementation example
  transform(value: any, ...args: any[]): any {
    return null;
  }*/

}
